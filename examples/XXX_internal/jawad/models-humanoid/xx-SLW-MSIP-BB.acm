// xx-SLW-MSIP-BB.acm
//
// Mass Spring Inverted Pendulum (MSIP) Model
// Virtual test to calculate Center of Mass profile
// with mass spring pendulum model and bang bang 
// control.
//
// Author: Jawad Masood
//
// Note: 
// Run using Semantics -> Traditional
//
// Model: 
// Single link mass spring inverted pendulum 
// with joint actuation, with one end fixed to 
// ground.
//
// Controller:
// Bang bang
//
// Scenario:
// Stationary upright walking
//
// Virtual experiment:
// This controller switches the joint torque to 
// create gait motion.
//
// Conclusion from virtual test: 
// Simulation suggests that the system does
// create a gait motion. It is difficult to create
// gait motion as function of time. 

class humanoid(b,k,m,l,g,T)
 private
 x   := 0;   // Link position in x-coordinate
 y   := 0;   // Link position in y-coordinate
 r   := 1;   // Radial length of link 
 r'  := 0;   // Radial velocity of link
 r'' :=0;    // Radial acceleration of link
 th  := pi/4;// Joint angle w.r.t y-axis
 th' := 0;   // Angular velocity
 th'':= 0;   // Angular acceleration
 _3D :=[];
 end
 
 // MSIP Dynamics (Euler-Lagrangian)
 r'' = r*((th')^2)-g*cos(th)+(k/m)*(l-r)-(b/m)*r';
 th'' = (g*sin(th)-2*r'*th')/(r) + (T/(m*(r)^2));
 
 // Simple bang bang control
  if th >= 0 && th<= pi/2
   T := -500;
  else
   T := 500; 
  end;
 
 // Forward kinematics
 x = r*sin(th);// X-coordinate transformation
 y = r*cos(th);// Y-coordinate transformation
 _3D = [["Sphere",[x,0,y],0.001*m,[1,0,0],[0,0,0]],
        ["Sphere",[0,0,0],0.1,[0,0,1],[0,th,0]],
        ["Cylinder",[0.5*sin(th),0,0.5*cos(th)],[0.01,1-(1-r)],[0,1,0],[pi/2,-th,0]]];        
end
class Main(simulator)
 private 
 sys := create humanoid(1/600,800000,60,1,9.81,0);
 end
 simulator.endTime = 5;
 simulator.timeStep = 0.001;
end