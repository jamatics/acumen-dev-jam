// xx-SLW-IP-BB.acm
//
// Inverted Pendulum Humanoid Model
// Virtual test to calculate Center of Mass 
// trajectory with inverted pendulum model and bang bang
// control.
//
// Author: Jawad Masood
//
// Note: 
// Run using Semantics -> Traditional
//
// Model: 
// Single link inverted pendulum with joint
// actuation, with one end fixed to ground.
//
// Controller:
// Bang bang
//
// Scenario:
// Stationary upright walking
//
// Virtual experiment:
// This controller switches the joint torque to 
// create gait motion.
//
// Conclusion from virtual test: 
// Simulation suggests that the system does
// create a gait motion. It is difficult to create
// gait motion as function of time.

class humanoid(m,l,T,g)
 private
 x   := 0;   // Link position in x-coordinate
 y   := 0;   // Link position in y-coordinate
 th'':= 0;   // Angular acceleration of link
 th' := 0;   // Angular velocity of link
 th  := pi/4;// Angle between link and x-axis
 _3D :=[];
 end

 // Single link dynamics (Euler-Lagrangian)
 th'' = -(g/l)*cos(th)+(T/(m*l^2));
 
 // Simple bang bang control
 if th >= 0 && th <= pi/2
  T := 500;
 else
  T := -500; 
 end;

 // Forward kinematics
 x   = l*cos(th);//X-coordinate transformation
 y   = l*sin(th);//Y-coordinate transformation
 _3D =[["Sphere",[x,0,y],0.001*m,[1,0,0],[0,th,0]],
      ["Sphere",[0,0,0],0.05,[0,0,1],[0,th,0]],
      ["Cylinder",[0.5*cos(th),0,0.5*sin(th)],[0.01,l],[0,1,0],[-th,0,pi/2]]];
end
class Main(simulator)
 private 
 sys := create humanoid(60,1,0,9.81);
 end
 simulator.endTime = 10;
 simulator.timeStep = 0.001;
end
