// MSIP Humanoid Model
// Virtual test with link compliance and visualization.
//
// Author: Jawad Masood
//
// Note: 
// Run using Semantics -> Traditional Functional
//
// Description: 
// This is a simple but extremely reliable model for 
// calculation center of mass trajectories, reaction 
// forces and spring forces in limb. The mass of the 
// 60 kg with initial torque of 50 Nm is considered.   
// The limb stiffness is considered as 800000 N/m and 
// limb muscle damping is consideres as 1/600 Ns/m. 
//
// Scenario:
// Initially robot leg is creating an initia; angle of
// pi/4 with vertical axis. This visual test will
// calculate how quick controller bring leg angle to 
// desire angle.
//
// Conclusion from virtual test: 
// Model seems fine and pass the domain of validaity
// such as if we do not apply torque there is no motion
// according to forth equation. It is interesting 
// to observe by changing k(stiffness) and 
// and damping(b) values. It took controller
// one second to reach zero with kp value 200. 
// Controller is stable.
// 
// Explanation and recommended follow up:
// The next step will be to design a PID controller
// with some tracking. 

 class humanoidMechanics(b,k,m,l,T)
  private
  x := 0; y := 0; z :=0;
  x1':= 0; x1 := 1; x2':= 0;x2 := 0;
  x3 := 0; x3' := 0;
  x4 := 0; x4' := 0;
  t' := 0; t :=0;
  g := 9.81;
  e := 0; et := 0; e1 := 0; 
  kp:=40;Kd:=0.1;ki:=200;
  x3_d :=0; x3_d1 := 0; i' :=0; i :=0;
  o:=0; p:=0;
  _3D:=[["Sphere",[1,0,0],0.01*3,[0,0,1],[0,0,0]],
        ["Sphere",[0,0,0],0.01*4,[0,0,1],[0,0,0]],
        ["Cylinder",[0,0,-0.5],[0.01,2],[0,1,0],[0,0,3.1416/2]]];
  end
  // Mechanics
   x1' = x2;
   x2' = x1*((x4)^2)-g*cos(x3)+(k/m)*(l-x1)-(b/m)*x2;
   x3' = x4;
   x4' = (g*sin(x3)-2*x2*x4)/(x1) + (T/(m*(x1)^2));
   // Tracking control desire configuration
  t'=1;
  if t>0 && t<= 1
  x3_d = 5*pi*t^3 - 7.5*pi*t^4 + 3*pi*t^5; 
  x3_d1= 15*pi*t^2 - 30*pi*t^3 + 15*pi*t^4;
  else 
   x3_d = 0.5*pi-5*pi*(t-1)^3 - 7.5*pi*(t-1)^4 - 3*pi*(t-1)^5;
   x3_d1 = -15*pi*(t-1)^2 + 30*pi*(t - 1)^3 - 15*pi*(t - 1)^4;
  end;

  // Torque control at joint which track the desire 
  // trajectory
  i' = et ;
  e = x3_d-x3;
  o = cos(e);
  p = sin(e);
  et = atan2(p,o); // To avoid angular error
  e1 = x3_d1-x3'; 
  T = (Kd + (1/(0.2)^2))*(e1 + kp * et + ki * i);
  // No need of trajectory generator and Path generator
  // Coordinate transformation
   x=x1*sin(x3);
   y=x1*cos(x3);
  _3D=[["Sphere",[x,0,-y],0.1*m,[0,0,1],[0,0,0]],
         ["Sphere",[0,0,0],0.1,[0,0,1],[0,x3,0]],
         ["Cylinder",[0.5*sin(x3),0,-0.5*cos(x3)],[0.01,1-(1-x1)],[0,1,0],[pi/2,x3,0]]];              
end
 class Main(simulator)
  private 
  mechanics := create humanoidMechanics(1/60,8000,1,1,15);
  end
  simulator.endTime = 1.45;
  simulator.timeStep = 0.001;
end