 
// MSIP Humanoid Model
// Virtual test to generate trajectory of COM
//
// Author: Jawad Masood
//
// Note: 
// Run using Semantics -> Traditional Functional
//
// Description: 
// This is a simple but extremely reliable model for 
// calculation center of mass trajectories, reaction 
// forces and spring forces in limb. The mass of the 
// 60 kg with initial torque of 50 Nm is considered.   
// The limb stiffness is considered as 8000 N/m and 
// limb muscle damping is consideres as 1/60 Ns/m. 
//
// Scenario:
// Initially robot leg is creating an initial angle of
// pi/4 with vertical axis. This visual test will
// calculate how quick controller bring leg angle to 
// desire angle.
//
// Conclusion from virtual test: 
// Model seems fine and pass the domain of validaity
// such as if we do not apply torque there is no motion
// according to fourth equation. It took controller
// one second to reach zero with kp value 200. 
// Controller is stable.
// 
// Explanation and recommended follow up:
// The next step will be to design a PID controller
// which switch motor torques when limb pass the step
// constraint i.e. x3 > pi/6 

class humanoid(b,k,m,l,T,g,kp)
 private 
 x1':= 0;
 x1 := 1;
 x2':= 0;
 x2 := 0;
 x3 := pi/4;
 x3' := 0;
 x4 := 0;
 x4' := 0;
 th_d := 0;
 th_d' := 0;
 e := 0;
 et := 0;
 x := 0;
 y := 0;
 _3D:=[["Sphere",[1,0,0],0.01*3,[0,0,1],[0,0,0]],
       ["Sphere",[0,0,0],0.01*4,[0,0,1],[0,0,0]],
       ["Cylinder",[0,0,-0.5],[0.01,2],[0,1,0],[0,0,3.1416/2]]];
 end
 // Humanoid Dynamics
 x1' = x2;
 x2' = x1*((x4)^2)-g*cos(x3)+(k/m)*(l-x1)-(b/m)*x2;
 x3' = x4;
 x4' = (g*sin(x3)-2*x2*x4)/(x1)+(T/(m*(x1)^2));
 // Control design
 e = th_d'-x3';
 et = atan2(sin(e),cos(e));
 T = et*kp; //P regulator
 // No need of trajectory generator and Path generator
 // Forward kinmetaics coordinate transformation
 x = x1*sin(x3);
 y = x1*cos(x3);
 _3D = [["Sphere",[x,0,y],0.001*m,[0,0,1],[0,0,0]],
        ["Sphere",[0,0,0],0.1,[0,0,1],[0,x3,0]],
        ["Cylinder",[0.5*sin(x3),0,0.5*cos(x3)],[0.01,1-(1-x1)],[0,1,0],[pi/2,-x3,0]]];
end

class Main(simulator)
 private 
 sys := create humanoid(1/60,800000,60,1,50,-9.81,200);
end
 simulator.endTime = 5;
end