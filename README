
ACUMEN 2012 PREVIEW RELEASE, RELEASE NOTES
------------------------------------------

This is a preview release of new features to come in 2013.  
New features since the last major Acumen release include:

 - 3D support based on Yingfu Zeng's Masters thesis
 - Enlcosure semantics with support for Zeno systems
 - A more responsive user interface
 - Some improvements to syntax (such as = and := )
 - Syntax highlighting

This is only a preview release, as there are a few known 
issues which we plan to fix in early 2013.

Some known issues:

  - Acumen 3D has known problems on Linux platforms
  - The system may need to be shut down externally

For users of Acumen 2010 please note the following important syntax
change:

  - [=] is now =  and
  - =   is now :=

PREREQUISITES
-------------

Users of recent version of Mac OS X should already have all necessary
dependencies installed.

If you are using fairly modern Linux distribution installing
openjdk-6, libjava3d, that comes with your distribution will likely
work fine.

If you are using Windows than follow instructions to get the necessary
requirements to run Acumem with 3D support.

If you are not running Windows 7, or running a windows 7-32 bits

  1. Download and install jre-7u7-windows-i586.exe from here
  http://www.oracle.com/technetwork/java/javase/downloads/jre7u7-downloads-1836441.html
  2. Download and install java3d-1_5_1-windows-i586.exe from here
  http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-java-client-419417.html#java3d-1.5.1-oth-JPR

If you are running a 64-bit windows 7, you need to uninstall your Java
32 bit first (if applicable) then

  1. Download jre-7u7-windows-x64.exe from here
  http://www.oracle.com/technetwork/java/javase/downloads/jre7u7-downloads-1836441.html
  2. Download and install java3d-1_5_1-windows-amd64.exe from here
  http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-java-client-419417.html#java3d-1.5.1-oth-JPR
  3. Follow the instructions about copying the files here
  http://www.christoph-lauer.de/Homepage/Blog/Eintrage/2012/5/23_Note__how_to_install_Java3D_on_a_64bit_Windows_7_machine.html

RUNNING
-------

To run acumen you should be able to run the provided jar file directly.

To run it from the command line something like:

  java -jar acumen-VERSION.jar

If running on Mac OS X, performance may suffer when using Acumen3D
unless the Java is started with UseQuartz enabled:

  java -Dapple.awt.graphics.UseQuartz=true -jar acumen-VERSION.jar

SUPPORT
-------

If you can not get acumen started please email help@acumen-language.org
with the following information:

  1) The acumen version you are using (which should be part of the
     name of the jar file).
  2) The name of the OS that you are using (Windows, Mac OS X, etc),
  3) Any specific version information you might have about the OS,
  4) The version of Java that you are using.  To check which version
     of Java you have, please use the website:  http://javatester.org/

If you are having problem with a particular model rather then getting
acumen started than also:

  5) The code you where trying to execute.

We also welcome your feedback on ways to make Acumen better, please
send and bug reports are just general suggestions to the same address
(help@acumen-language.org).

COMPILING
---------

To compile from source you need to install SBT.  The easiest thing is
to download this file:
  http://repo.typesafe.com/typesafe/ivy-releases/org.scala-sbt/sbt-launch/0.12.0/sbt-launch.jar
and save it to ~/bin/sbt-launch.jar.  Then, create a shell script
called sbt with the following contents
  java -Xmx2g -XX:MaxPermSize=1g -XX:ReservedCodeCacheSize=128m -jar ~/bin/sbt-launch.jar "$@"

Then to compile and run acumen use:

  sbt run

To create a jar file use:

  sbt proguard

which will create a jar file in target/scala-2.9.2/acumen-VERSION.jar

To do more read up on how how to use sbt at
https://github.com/harrah/xsbt/wiki.
